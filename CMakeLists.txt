cmake_minimum_required(VERSION 3.14)
project(udp-client C)

set(CMAKE_C_STANDARD 99)

add_executable(udp-client main.c)

if(WIN32)
    target_link_libraries(udp-client wsock32 ws2_32)
endif()
