#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <winsock2.h>
#include <libgen.h>

#define BUFFER_SIZE 4096 // must be same in server
#define PATH_MAX 4096
#define FLAG 0

FILE * process_file(unsigned char* buffer, unsigned long file_size);

int main(int argc, char **argv) {

    int socketFD;
    int port_number;
    int server_addr_len;
    unsigned long nBytes = 0;
    unsigned long file_size;
    unsigned long big_endian;

    char *ip_address;
    unsigned char buffer[BUFFER_SIZE];

    FILE *fp;
    WSADATA wsaData;
    struct sockaddr_in server_addr;


    if(argv[1] == NULL){
        printf("no server ip address declared");
        exit(EXIT_FAILURE);
    }

    if(argv[2] == NULL){
        printf("no server port address declared");
        exit(EXIT_FAILURE);
    }

    server_addr_len = sizeof(server_addr);
    ip_address = argv[1];
    port_number = atoi(argv[2]);

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(port_number);
    server_addr.sin_addr.s_addr = inet_addr(ip_address);


    if (WSAStartup(MAKEWORD(1, 1), &wsaData) == SOCKET_ERROR) {
        printf("Error initialising WSA.\n");
        return -1;
    }

    socketFD = socket(AF_INET, SOCK_DGRAM, 0);

    if (socketFD < 0) {
        printf("file descriptor not received!!\n");
        perror("Error %d opening socket.\n");
        return -1;
    } else
        printf("file descriptor %d received\n", socketFD);
    {

        printf("Please enter file name to receive: (absolute path of file on server)\n");
        scanf("%s", buffer);

        // sending file name
        sendto(socketFD, (char *)buffer, BUFFER_SIZE, FLAG, (struct sockaddr *) &server_addr, server_addr_len);
        //receiving file size
        recvfrom(socketFD, (char *)&big_endian, sizeof(big_endian), FLAG, (struct sockaddr *) &server_addr, &server_addr_len);
        file_size = ntohl(big_endian);

        printf("Server information:\n");
        printf("\tServer Ip : %s\n", ip_address);
        printf("\tServer Port : %d\n\n", port_number);

        fp = process_file(buffer,file_size);

        unsigned long loop_num = file_size / BUFFER_SIZE;

        for (int i = 1; i <= loop_num; i++) {

            memset(buffer, '\0', BUFFER_SIZE);
            nBytes += recvfrom(socketFD, (char *)buffer, BUFFER_SIZE, FLAG, (struct sockaddr *) &server_addr, &server_addr_len);
            sendto(socketFD, "ACK", BUFFER_SIZE, FLAG, (struct sockaddr *) &server_addr, server_addr_len);

            if(argv[3] != NULL) {
                if (strcmp("on", argv[3]) == 0) // verbose mode
                    printf("\nReceived packet :\n%s\n\n", buffer);
            }
            fwrite(buffer, sizeof(char), BUFFER_SIZE, fp);
        }

        nBytes += recvfrom(socketFD, (char *)buffer, BUFFER_SIZE, FLAG, (struct sockaddr *) &server_addr, &server_addr_len);
        if(file_size == 0){
            printf("Server message : %s", buffer);
            exit(1);
        }
        sendto(socketFD, "ACK", BUFFER_SIZE, FLAG, (struct sockaddr *) &server_addr, server_addr_len);
        if(argv[3] != NULL) {
            if (strcmp("on", argv[3]) == 0) // verbose mode
                printf("\nReceived packet :\n %s\n\n", buffer);
        }
        fwrite(buffer, sizeof(char), file_size % BUFFER_SIZE, fp);

        nBytes = nBytes - (BUFFER_SIZE - file_size % BUFFER_SIZE);
        printf("\nReceived %lu bytes", nBytes);

        printf("\n-------------------------------\n");
        if (fp != NULL)
            fclose(fp);

    }
    return 0;
}

FILE * process_file(unsigned char* buffer, unsigned long file_size){

    char cwd[PATH_MAX];
    FILE * fp = NULL;

    if (getcwd(cwd, sizeof(cwd)) == NULL) {
        perror("can not get current directory\n");
        exit(EXIT_FAILURE);
    }

    char *base_name;
    base_name = basename((char *)buffer);
    char file_abs_path[2048];
    memset(file_abs_path, '\0', sizeof(file_abs_path));


    if (file_size != 0) {
        printf("Saving file in : \n");
        printf("\tDirectory : %s\n", cwd);
        printf("\tFile : %s\n", base_name);
        printf("\tSize : %lu B\n", file_size);

        strcat(file_abs_path, cwd);
        strcat(file_abs_path, "\\");
        strcat(file_abs_path, base_name);
        printf("\tAbsolute Path : %s\n", file_abs_path);
        if ((fp = fopen(file_abs_path, "wb")) == NULL) {
            perror("cant no opening file\n");
            exit(1);
        }
    }
    return fp;
}
